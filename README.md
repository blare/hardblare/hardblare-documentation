**Author:** Mounir NASR ALLAH <mounir@nasrallah.fr>

**Date:** June 21th, 2021

# Table of contents

1. [Introduction](#introduction)
2. [Environnement](#environnement)
	- [Operating system](#operating-system)
	- [Vivado](#vivado)
	- [Yocto depedencies](#yocto-depedencies)
	- [Directory Hierarchy](#directory-hierarchy)
	- [Creating an SSH Key](#creating-an-ssh-key)
	- [Cloning Git repositories](#cloning-git-repositories)
3. [Hardware](#hardware)
	- [Hardblare_hw](#hardblare_hw)
	- [The coprocessor firmware](#the-coprocessor-firmware)
	- [The TMC project](#the-tmc-project)
4. [Software](#software)
	- [Yocto directory](#yocto-directory)
	- [Source code](#source-code)
	- [Distribution compilation](#distribution-compilation)
5. [ZedBoard FPGA](#zedboard-fpga)
	- [How to prepare the SD Card](#how-to-prepare-the-sd-card)
	- [How to configure the ZedBoard with the jumpers](#how-to-configure-the-zedboard-with-the-jumpers)
	- [Connecting and starting the ZedBoard card](#connecting-and-starting-the-zedboard-card])
6. [How to use HardBlare](#how-to-use-hardblare)
	- [SDK compilation](#sdk-compilation)
	- [Deploying the SDK](#deploying-the-sdk)
	- [Using the SDK](#using-the-sdk)
	- [Source code of use cases](#source-code-of-use-cases)
	- [How to use the annotations generator](#how-to-use-the-annotations-generator)


# Introduction

The HardBlare project contains two parts: the hardware part described into the [Hardware](#hardware) section, and the software part described into the [Software](#software) section.

For generating a full distribution for the HardBlare project, we need severals components and seting up a proper environnement described into the [Environnement](#environnement).


# Environnement

In order to avoid any compatibility problem between the different versions of the applications, it is necessary to use the same versions used during the development of HardBlare

## Operating system

The distribution used for developping HardBlare is the Ubuntu distribution 16.04

```
uname -a
Linux HardBlare-Workstation 4.15.0-128-generic #131~16.04.1-Ubuntu SMP Wed Dec 9 17:33:47 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
```

![](img/Ubuntu.png) 

You can find the distribution image on the [link](https://ubuntu.com/16-04).

## Vivado

The hardware part was developed with the Xilinx tool: Vivado 2018.2

![](img/vivado.png) 

The installer file is available on the Xilinx website at the ["Vivado Archive"](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vivado-design-tools/archive.html) page.


## Yocto depedencies

The following command list the required packages needed to build an image on Ubuntu.

```shell
sudo apt-get install gawk wget git-core diffstat unzip \
	texinfo gcc-multilib build-essential chrpath socat cpio \
	python python3 python3-pip python3-pexpect xz-utils software-properties-common  debianutils \
	iputils-ping libsdl1.2-dev xterm clang
```

### Install specific GCC version
A specific GCC version is needed which is not present in default packages on a Ubuntu 16. We need to add another PPA:
```bash
sudo add-apt-repository ppa:ubuntu-toolchain-r/test -y
sudo apt update
sudo apt install gcc-6 g++-6
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-6 60 --slave /usr/bin/g++ g++ /usr/bin/g++-6
```

## Directory Hierarchy 

The first thing to begin with is to create a directory called "HardBlare" in **your home repository**. Everything needed will be present in this directory. In the rest of this documentation, we will refer to "HardBlare" for the directory mentionned above.

Open a shell terminal, and go to your home directory, then type commands bellows.

```shell
mkdir HardBlare
cd ~/HardBlare
```

## Creating an SSH Key
For cloning the repositories of the HardBlare project, you need to create an asymetric SSH key for each computer you working on. 

Open a terminal and execute the command bellow for generating a new ssh key:

```shell
[~]$ ssh-keygen
```

By default, the command create two files called id_rsa (private key) and id_rsa.pub (public key).

Go to [gitlab.inria.fr](gitlab.inria.fr), login with your own credentials, then click on "User Settings" [https://gitlab.inria.fr/-/profile/keys](https://gitlab.inria.fr/-/profile/keys), then go to "SSH Keys ". Click on "Add Key" or copy/paste the content of your public key or import it (id_rsa.pub).


## Cloning Git repositories


After being in your HardBlare directory, you need to clone some git repo present in the hardblare's Gitlab project. 

Go to [https://gitlab.inria.fr/blare/hardblare](https://gitlab.inria.fr/blare/hardblare), login with your own credentials.

In the rest of this article, we suppose that your login's credentials is **user**. 

# Hardware

All the code needed for the hardware part is in the hardblare_hw repository. 

## hardblare_hw 

Clone this repository into **~/HardBlare/**

```shell
[~/HardBlare/]$ git clone git@gitlab.inria.fr:blare/hardblare/hardblare-hw.git 
[~/HardBlare/]$ cd hardblare-hw
```

Branches :

|  Name | Status |  Description |  TODO |
|-------|--------|--------------|-------|
| master|  ![alt text](https://img.shields.io/badge/build-passing-green.svg "Build") ![alt text](https://img.shields.io/badge/working-stable-green.svg "Build")  | The only branch containing the stable version |  Nothing |


The content of this repo is detailed in the following table :

|  Folder Name | Description | 
|-------------------|----------------|
| tmc_project | This is the main folder containing the main file for the overall hardware architecture.   |
| firmware_coprocessor | This folder contain the firmware user by the Hardware.  |  
| device-tree | This folder contain the device tree, uEnv.txt and the FSBL  | 
| IPs | This folder contain all hardware components .   | 
| IPs/Sources | This sub-folder contain all sources (VHDL files) used by hardware components. | 
| IPs/Sources/BRAM_to_FIFO | This component makes it possible to connect FIFO and to interact with these FIFO as if it were a BRAM.  | 
| IPs/Sources/PTM Decoder | This is the PTM decoder | 
| IPs/Sources/Plasma_MIPS | This is the Coprocessor based on a Plasma MIPS (https://opencores.org/projects/plasma) | 
| IPs/Sources/PLtoPS | This is the component for generating interrupts from PL to PS. This component also exposes the reasons and information about the interruption. | 
| IPs/Packages | This sub-folder contain all "Vivado packages" used by hardware components. In order to have a clean VHDL code of components, the source code is stored into the IPs/Sources subfolder then imported into the respective package.   |  
| IPs/Packages/BRAM_to_FIFO | This component makes it possible to connect FIFO and to interact with these FIFO as if it were a BRAM.  |  
| IPs/Packages/decodeur_traces_v1_0_project | This is the PTM decoder |  
| IPs/Packages/Dispatcher_Plasma_MIPS_HardBlare | This is the Coprocessor based on a Plasma MIPS (https://opencores.org/projects/plasma) | 
| IPs/Packages/PL_to_PS | This is the component for generating interrupts from PL to PS. This component also exposes the reasons and information about the interruption. | 



## The coprocessor firmware

In order to perform information flow tracking into the coprocessor, a firmware is needed. That firmware is directly included into the FPGA memory.
That's why the firmware COE file is **NEEDED** for generating the bitstream file of the tmc_project.
This folder contains the firmware used by the coprocessor.
To compile the firmware, the make command is used.

```
[~/HardBlare/hardblare-hw/firmware_coprocessor/]$ make
```

Here is the output of this command.

```
~/HardBlare/hardblare-hw/firmware_coprocessor/cross-compiler-mips/bin/mips-as -o boot.o  ~/HardBlare/hardblare-hw/firmware_coprocessor/plasma/tools/boot.asm
~/HardBlare/hardblare-hw/firmware_coprocessor/cross-compiler-mips/bin/mips-gcc -O2 -Wall -c -s -I~/HardBlare/hardblare-hw/firmware_coprocessor/cross-compiler-mips/lib -fno-pic -mips1 -mno-abicalls main_firmware.c
~/HardBlare/hardblare-hw/firmware_coprocessor/cross-compiler-mips/bin/mips-gcc -O2 -Wall -c -s -I~/HardBlare/hardblare-hw/firmware_coprocessor/cross-compiler-mips/lib -fno-pic -mips1 -mno-abicalls hash_table.c
~/HardBlare/hardblare-hw/firmware_coprocessor/cross-compiler-mips/bin/mips-gcc -O2 -Wall -c -s -I~/HardBlare/hardblare-hw/firmware_coprocessor/cross-compiler-mips/lib -fno-pic -mips1 -mno-abicalls debug.c
~/HardBlare/hardblare-hw/firmware_coprocessor/cross-compiler-mips/bin/mips-gcc -O2 -Wall -c -s -I~/HardBlare/hardblare-hw/firmware_coprocessor/cross-compiler-mips/lib -fno-pic -mips1 -mno-abicalls address.c
~/HardBlare/hardblare-hw/firmware_coprocessor/cross-compiler-mips/bin/mips-gcc -O2 -Wall -c -s -I~/HardBlare/hardblare-hw/firmware_coprocessor/cross-compiler-mips/lib -fno-pic -mips1 -mno-abicalls decode.c
decode.c: In function 'decode_ti_format':
decode.c:740: warning: unused variable 'armOpcodeType'
~/HardBlare/hardblare-hw/firmware_coprocessor/cross-compiler-mips/bin/mips-gcc -O2 -Wall -c -s -I~/HardBlare/hardblare-hw/firmware_coprocessor/cross-compiler-mips/lib -fno-pic -mips1 -mno-abicalls memory_tag_trf.c
~/HardBlare/hardblare-hw/firmware_coprocessor/cross-compiler-mips/bin/mips-gcc -O2 -Wall -c -s -I~/HardBlare/hardblare-hw/firmware_coprocessor/cross-compiler-mips/lib -fno-pic -mips1 -mno-abicalls irq.c
~/HardBlare/hardblare-hw/firmware_coprocessor/cross-compiler-mips/bin/mips-gcc -O2 -Wall -c -s -I~/HardBlare/hardblare-hw/firmware_coprocessor/cross-compiler-mips/lib -fno-pic -mips1 -mno-abicalls  ~/HardBlare/hardblare-hw/firmware_coprocessor/plasma/tools/no_os.c
~/HardBlare/hardblare-hw/firmware_coprocessor/cross-compiler-mips/bin/mips-ld -Ttext 0x00 -e entry -Map test.map -s -N -o test.axf \
boot.o main_firmware.o no_os.o decode.o memory_tag_trf.o address.o debug.o irq.o
~/HardBlare/hardblare-hw/firmware_coprocessor/plasma/tools/convert_bin.exe
test.axf -> code.txt & test.bin
Entry=0x0 [vaddr=0x0,offset=0xa0,filesz=0x1a70,memsz=0x3cb8,flags=0x7]
length = 6768 = 0x1a70
length=6768=0x1a70
~/HardBlare/hardblare-hw/firmware_coprocessor/cross-compiler-mips/bin/mips-objdump --disassemble test.axf > test.lst
cp code.txt plasma/vhdl
cp code.txt ~/HardBlare/hardblare-hw/IPs/Sources/Plasma_MIPS 
echo -n "memory_initialization_radix=16;\nmemory_initialization_vector= " > plasma/vhdl/firmware_code.coe
cat plasma/vhdl/code.txt | tr '\r\n' ' ' >> plasma/vhdl/firmware_code.coe
echo ";"  >> plasma/vhdl/firmware_code.coe
cp plasma/vhdl/firmware_code.coe ~/HardBlare/hardblare-hw/IPs/Sources/Plasma_MIPS

```

The executable file is then transformed in order to be used by vivado in COE format (firmware_code.coe)
For uploading this firmware file, open the tmc_project in Vivado, select the BRAM_Firmware_code component, go to "Other Options" / "Memory initialization" and find the COE file. 

![](img/update_firmware_code.png) 

## The TMC project

The tmc_project contains all components connected needed for the FPGA part. 

Open Vivado 2018.2, click on "Open Project" and select the 
![](img/open_project_tmc_vivado.png) 

To generate the project bitstream file, click on the button "Generate Bitstream".
 ![](img/generate_bitstream.png) 

The resulting Bitstream file should later be load into the FPGA part.
The detailed architecture of the tmc_project can be found into [img/design_tmc_project.pdf](img/design_tmc_project.pdf).

# Software

Git repo are splitted in two kind, the first one is git repo wich contains "yocto recipies" or tools for generating the distribution. The second one is the source code of linux kernel, compiler, or applications wich are integrated into the final distribution.


## Yocto directory

We put all git repo related to Yocto recipes and tools into a directory named "Yocto", which itself is in the HardBlare directory.

```shell
[~/HardBlare]$ mkdir Yocto
[~/HardBlare]$ cd Yocto
```

### Poky for hardblare

For generating a distribution, we are using some tools from Yocto project. Thoses tools are included in the hardblare-yocto-poky-hardblare.git

Clone this repository into **~/HardBlare/Yocto**.

```shell
[~/HardBlare/Yocto]$ cd ~/HardBlare/Yocto 
[~/HardBlare/Yocto]$ git clone git@gitlab.inria.fr:blare/hardblare/hardblare-yocto-poky-hardblare.git poky-hardblare
[~/HardBlare/Yocto]$ cd poky-hardblare
```

Branches :

|  Name | Status |  Description |
|-------|--------|--------------|
| master|  ![alt text](https://img.shields.io/badge/build-passing-green.svg "Build") ![alt text](https://img.shields.io/badge/working-stable-green.svg "Build")  | The only branch containing the stable version |  


### Recipes of meta-openembedded
The linux distribution needs applications compilers, web server, SSH server, etc.
Those tools are provided by meta-openembedded

Clone this repository into **~/HardBlare/Yocto**.

```shell
[~/HardBlare/Yocto]$ cd ~/HardBlare/Yocto 
[~/HardBlare/Yocto]$ git clone git@gitlab.inria.fr:blare/hardblare/hardblare-yocto-meta-openembedded.git meta-openembedded
[~/HardBlare/Yocto]$ cd meta-openembedded
[~/HardBlare/Yocto/meta-openembedded]$ git fetch --all
[~/HardBlare/Yocto/meta-openembedded]$ git checkout --track origin/pyro
```
Branches :

|  Name | Status |  Description | 
|-------|--------|--------------|
| pyro|  ![alt text](https://img.shields.io/badge/build-passing-green.svg "Build") ![alt text](https://img.shields.io/badge/working-stable-green.svg "Build")  | The only branch containing the stable version |  

### Recipes for HardBlare 
The distribution generated for HardBlare is a set of recipes. Those recipes are among others user for selecting the configuration of the Linux Kernel or including applications needed for HardBlare. 

Clone this repository into **~/HardBlare/Yocto**.

```shell
[~/HardBlare/Yocto]$ cd ~/HardBlare/Yocto 
[~/HardBlare/Yocto]$ git clone git@gitlab.inria.fr:blare/hardblare/hardblare-yocto-meta-hardblare.git meta-hardblare
[~/HardBlare/Yocto]$ cd meta-hardblare
[~/HardBlare/Yocto/meta-hardblare]$ git fetch --all
```
Branches :

|  Name | Status |  Description | 
|-------|--------|--------------|
| master|  ![alt text](https://img.shields.io/badge/build-passing-green.svg "Build") ![alt text](https://img.shields.io/badge/working-stable-green.svg "Build")  | The only branch containing the stable version | 

### Recipes for Clang

This git repo allow us to compile the modified LLVM and Clang and including the modified compiler for HardBlare into the SDK. 

Clone this repository into **~/HardBlare/Yocto** 

```shell
[~/HardBlare/Yocto]$ cd ~/HardBlare/Yocto
[~/HardBlare/Yocto]$ git clone git@gitlab.inria.fr:blare/hardblare/hardblare-yocto-meta-clang.git meta-clang-hardblare
[~/HardBlare/Yocto]$ cd meta-clang-hardblare
[~/HardBlare/Yocto/meta-clang-hardblare]$ git fetch --all

```

Branches :

|  Name | Status |  Description |
|-------|--------|--------------|
| master|  ![alt text](https://img.shields.io/badge/build-passing-green.svg "Build") ![alt text](https://img.shields.io/badge/working-stable-green.svg "Build")  | The only branch containing the stable version |


### Recipes for QEMU

Clone this repository into **~/HardBlare/Yocto**.

```shell
[~/HardBlare/Yocto]$ cd ~/HardBlare/Yocto 
[~/HardBlare/Yocto]$ git clone git@gitlab.inria.fr:blare/hardblare/hardblare-yocto-qemu-xilinx-hardblare.git  qemu-xilinx-hardblare
[~/HardBlare/Yocto]$ cd qemu-xilinx-hardblare
[~/HardBlare/Yocto/qemu-xilinx-hardblare]$ git fetch --all
[~/HardBlare/Yocto/qemu-xilinx-hardblare]$ git checkout --track origin/dev
```
Branches :

|  Name | Status |  Description | 
|-------|--------|--------------|
| master|  ![alt text](https://img.shields.io/badge/build-passing-green.svg "Build") ![alt text](https://img.shields.io/badge/working-stable-green.svg "Build")  | The only branch containing the stable version |



### Conclusion

The folder structure of the Yocto folder containing all the recipes needed to generate a linux distribution must be as follows: 
![](img/folder_yocto.png) 


## Source code

### Linux Kernel for HardBlare

This repo contains the merge of [linux-xlnx-v2017.1](https://github.com/Xilinx/linux-xlnx/releases/tag/xilinx-v2017.1) (Linux 4.9 from Xilinx) and the Linux Kernel 4.9 from RfBlare [https://www.blare-ids.org/rfblare/](https://www.blare-ids.org/rfblare/).
This Linux Kernel is also modified for including HardBlare features.

Clone this repository into **~/HardBlare**.

```shell
[~/HardBlare]$ cd ~/HardBlare/ 
[~/HardBlare]$ git clone git@gitlab.inria.fr:blare/hardblare/hardblare-kernel.git kblare-linux-4.9-hardblare
[~/HardBlare]$ cd kblare-linux-4.9-hardblare
[~/HardBlare/kblare-linux-4.9-hardblare]$ git fetch --all
[~/HardBlare/kblare-linux-4.9-hardblare]$ git checkout --track origin/dev
```

Branches :

|  Name | Status |  Description |
|-------|--------|--------------|
| master|  ![alt text](https://img.shields.io/badge/build-passing-green.svg "Build") ![alt text](https://img.shields.io/badge/working-stable-green.svg "Build") | This branch contains released stable version |
| dev|  ![alt text](https://img.shields.io/badge/build-passing-green.svg "Build") ![alt text](https://img.shields.io/badge/working-unstable-orange.svg "Build") | This branch contains new modification and could be unstable |


### LLVM for HardBlare

We are using a modified [LLVM 4.0.1](https://github.com/llvm-mirror/llvm/tree/release_40) 
(branch: release_40 
last commit: c8fccc53ed66d505898f8850bcc690c977a7c9a7). In this repo, we have developed an instrumentation pass, a static analysis for generating annotations, and we have modified the structure of the ELF file generated for storing annotations and needed information into dedicated HardBlare ELF sections and segments.

Clone this repository into **~/HardBlare**.

```shell
[~/HardBlare]$ cd ~/HardBlare/ 
[~/HardBlare]$ git clone git@gitlab.inria.fr:blare/hardblare/hardblare-llvm.git llvm
[~/HardBlare]$ cd llvm
[~/HardBlare/llvm]$ git fetch --all
[~/HardBlare/llvm]$ git checkout --track origin/dev
```

Branches :

|  Name | Status |  Description | 
|-------|--------|--------------|
| master|  ![alt text](https://img.shields.io/badge/build-passing-green.svg "Build") ![alt text](https://img.shields.io/badge/working-stable-green.svg "Build") | This branch contains released stable version |
| dev|  ![alt text](https://img.shields.io/badge/build-passing-green.svg "Build") ![alt text](https://img.shields.io/badge/working-unstable-orange.svg "Build") | This branch contains new modification and could be unstable |


### Clang for HardBlare

We are using a modified [Clang 4.0.1](https://github.com/llvm-mirror/clang/tree/release_40)
(branch: release_40 
last commit: 3c8961bedc65c9a15cbe67a2ef385a0938f7cfef).

Clone this repository into **~/HardBlare**.

```shell
[~/HardBlare]$ cd ~/HardBlare/ 
[~/HardBlare]$ git clone git@gitlab.inria.fr:blare/hardblare/hardblare-clang.git clang
[~/HardBlare]$ cd clang
[~/HardBlare/clang]$ git fetch --all
[~/HardBlare/clang]$ git checkout --track origin/dev
```

Branches :

|  Name | Status |  Description | 
|-------|--------|--------------|
| master|  ![alt text](https://img.shields.io/badge/build-passing-green.svg "Build") ![alt text](https://img.shields.io/badge/working-stable-green.svg "Build") | This branch contains released stable version |
| dev|  ![alt text](https://img.shields.io/badge/build-passing-green.svg "Build") ![alt text](https://img.shields.io/badge/working-unstable-orange.svg "Build") | This branch contains new modification and could be unstable |



### Kblare tools for HardBlare

Kblare tools contains all user application for tagging files.
Clone this repository into **~/HardBlare**.


```shell
[~/HardBlare]$ cd ~/HardBlare/ 
[~/HardBlare]$ git clone git@gitlab.inria.fr:blare/hardblare/hardblare-kblare-tools.git kblare-tools
[~/HardBlare]$ cd kblare-tools
[~/HardBlare/kblare-tools]$ git fetch --all
[~/HardBlare/kblare-tools]$ git checkout --track origin/dev
```

Branches :

|  Name | Status |  Description |
|-------|--------|--------------|
| master|  ![alt text](https://img.shields.io/badge/build-passing-green.svg "Build") ![alt text](https://img.shields.io/badge/working-stable-green.svg "Build") | This branch contains released stable version |
| dev|  ![alt text](https://img.shields.io/badge/build-passing-green.svg "Build") ![alt text](https://img.shields.io/badge/working-unstable-orange.svg "Build") | This branch contains new modification and could be unstable |


### Binutils-gdb for HardBlare

Binutils contains a modified version of binutils-gdb tools for supporting HardBlare ELF sections and segments.
Clone this repository into **~/HardBlare**.


```shell
[~/HardBlare]$ cd ~/HardBlare/ 
[~/HardBlare]$ git clone git@gitlab.inria.fr:blare/hardblare/hardblare-binutils.git  binutils-gdb
[~/HardBlare]$ cd binutils-gdb
[~/HardBlare/binutils-gdb]$ git fetch --all
[~/HardBlare/binutils-gdb]$ git checkout --track origin/dev
```

Branches :

|  Name | Status |  Description | 
|-------|--------|--------------|
| master|  ![alt text](https://img.shields.io/badge/build-passing-green.svg "Build") ![alt text](https://img.shields.io/badge/working-stable-green.svg "Build") | This branch contains released stable version |
| dev|  ![alt text](https://img.shields.io/badge/build-passing-green.svg "Build") ![alt text](https://img.shields.io/badge/working-unstable-orange.svg "Build") | This branch contains new modification and could be unstable |



### Musl LibC for HardBlare

For using HardBlare, the C standard library has to be modified. Those modification are mainly used for setting up and initiate communication with the coprocessor in order to configure the information policy for example.
We have chosen to use musl libC as defaut C standard library for HardBlare.

Clone this repository into **~/HardBlare**.

```shell
[~/HardBlare]$ cd ~/HardBlare/ 
[~/HardBlare]$ git clone git@gitlab.inria.fr:blare/hardblare/hardblare-musl-libc.git musl-libc-hardblare/
[~/HardBlare]$ cd musl-libc-hardblare/
[~/HardBlare/musl-libc-hardblare/]$ git fetch --all
[~/HardBlare/musl-libc-hardblare/]$ git checkout --track origin/dev
```

Branches :

|  Name | Status |  Description | 
|-------|--------|--------------|
| master|  ![alt text](https://img.shields.io/badge/build-passing-green.svg "Build") ![alt text](https://img.shields.io/badge/working-stable-green.svg "Build") | This branch contains released stable version |
| dev|  ![alt text](https://img.shields.io/badge/build-passing-green.svg "Build") ![alt text](https://img.shields.io/badge/working-unstable-orange.svg "Build") | This branch contains new modification and could be unstable |



# Distribution compilation

For building a complete distribution, go to the "Yocto/poky-hardblare" repository. And then source the "oe-init-build-env" file.

```shell
[~/HardBlare]$ cd ~/HardBlare/Yocto/poky-hardblare
[~/HardBlare/Yocto/poky-hardblare]$ source oe-init-build-env
```

Finaly, execute the bitbake command for building the distribution. 

```
[~/HardBlare/Yocto/poky-hardblare]$ bitbake core-image-minimal
```

At the end of the compilation, the kernel image *(uImage-zedboard-hardblare.bin)* and the root file system *(core-image-minimal-zedboard-hardblare.ext4)* will be generated.

```
[~/HardBlare/Yocto/poky-hardblare/build]$ ls tmp/deploy/images/zedboard-hardblare/
core-image-minimal-zedboard-hardblare.ext4
uImage-zedboard-hardblare.bin
...

```


# ZedBoard FPGA

The HardBlare project has been developed to work only on a ZedBoard FPGA board.
The ZedBoard is a low-cost development board for the Xilinx Zynq-7000 programmable SoC. The Zynq-7000 SoCs contains ARM® processing system (called PS) and a 7-series programmable logic (called PL). 

The board is similar to the picture bellow. The configuration of the board is detailed in the following sections.
![](img/zedboard_config.jpg) 

## How to prepare the SD Card

The first step is to load all necessary files into the SD Card.
Two partitions are needed on the SD card. 

This first one (FAT Format) will be used to put the uEnv.txt, the FSBL, the linux kernel image and the device tree.
The second one (ext4 Format) will be used to hold the root file system.

Format your SD Card with your favorite disk utility.

![](img/disk_utility.png) 

Then, we are using fdisk for creating partitions.
The first step is to locate the path of your SD Card in your system. 
```
[~/HardBlare]$ sudo fdisk -l
```
![](img/fdisk-list.png) 

The SD Card is located in /dev/sde, change it by your own path.

```
[~/HardBlare]$ sudo fdisk /dev/sde
```

 If you type p, you will see your current partition table. 

![](img/fdisk_p.png) 


Type 'n' in order to create a new partition:

![](img/fdisk_n.png) 


Make it primary by selecting 'p', use default partition number and default first sector . Set aside 1G for this partition by typing '+1G'.

![](img/fdisk_first_partition.png) 

Make this partition bootable. Set bootable flag by typing 'a'.

![](img/fdisk_a.png) 

Create the second partition by typing the command 'n'
Use the default values for the partition type (primary), the partition number (2), the first sector and the last sector.

![](img/second_partition.png) 


If you check your partition table now, you should see the two partitions that you've just created. Type the command 'p' to verify.

![](img/fdisk_end_p.png) 


Type the command 'w' to write to disk and exit.

![](img/fdisk_w.png) 

Now we need to format partitions.
The first partition called BOOT (1GB) needs to be in FAT format. Run the command bellow.

```
[~/HardBlare]$ sudo mkfs.vfat -F 32 -n BOOT /dev/sde1
```

![](img/format_first_partition.png) 


The second partition called ROOT should be in ext4 format. Run the command bellow:

```
[~/HardBlare]$ sudo mkfs.ext4 -L ROOT /dev/sde2
```
![](img/format_second_partition.png) 


### Files into the first partition

In order to know the path of the BOOT partition run the command bellow:

```
[~/HardBlare]$ df -h /dev/sde1
```

![](img/mounted_point_first_partition.png) 

Copy those file into the root of the boot partition:
```
[~/HardBlare]$ cp ~/HardBlare/hardblare-hw/uEnv.txt /media/mounir/BOOT
[~/HardBlare]$ cp ~/HardBlare/hardblare-hw/FSBL/boot.bin /media/mounir/BOOT
[~/HardBlare]$ cp ~/HardBlare/hardblare-hw/device-tree/zynq-zed.dtb /media/mounir/BOOT
```
And copy the kernel image generated by yocto in the section [Poky distribution compilation](#poky-distribution-compilation). Rename the kernel image by uImage.bin

```
[~/HardBlare]$ cp ~/HardBlare/Yocto/poky-hardblare/build/tmp/deploy/images/zedboard-hardblare/uImage-zedboard-hardblare.bin /media/mounir/BOOT/uImage.bin
```

### Files for the second partition 

The root file system should be extracted to the second partition called "ROOT". For that purpose, we are using "dd". The command needs to be modified by the path of your rootfs file and the path of your partition:

```
[~/HardBlare]$ sudo dd if=~/HardBlare/Yocto/poky-hardblare/build/tmp/deploy/images/zedboard-hardblare/core-image-minimal-zedboard-hardblare.ext4 dd=/dev/sde2
```

![](img/copy_rootfs_ext4.png) 

## How to configure the ZedBoard with the jumpers

The jumpers of the ZedBoard card should be connected as follow:

-  **MI06, JP10, JP9, MI03, MI02**

	![](img/jumper_MI_JP.jpg) 

-  **2V5 for VADJ SELECT**

	![](img/jumper_VADJ_SELECT.jpg) 

-  **JP2 near of the USB OTG Port**

	![](img/jumper_JP2.jpg) 


## Connecting and starting the ZedBoard card

Connect the power supply cable to the card. Connect an USB cable to your computer and to the UART USB Port of the ZedBoard.
Turn on the ZedBoard.

![](img/zedboard_power_usb.jpg)

Launch your favorite serial application for connecting to the remote serial console.
The linux distribution should then start. The default credentials are:

- **user:** root
- **password:** root

The **animation bellows** show the boot process from turning on the board to the logging in.

![](img/bootime.gif)


# How to use HardBlare

First of all, we need a complete compilation tools chains for making user applications suitable for HardBlare. 

## SDK compilation

For building an SDK, go to the "Yocto/poky-hardblare" repository. And then source the "oe-init-build-env" file.

```shell
[~/HardBlare]$ cd Yocto/poky-hardblare
[~/HardBlare/Yocto/poky-hardblare]$ source oe-init-build-env
```

Finaly, execute bitbake command bellow for building the SDK. 

```
[~/HardBlare/Yocto/poky-hardblare]$ bitbake core-image-minimal -c populate_sdk
```

## Deploying the SDK

Once the SDK compilation is completed, a package is created into the directory *"~/HardBlare/Yocto/poky-hardblare/build/tmp/deploy/sdk"*.

It is then necessary to run this script in order to install the SDK on the computer.

```
[~/HardBlare/Yocto/poky-hardblare/build]$ cd tmp/deploy/sdk 

[~/HardBlare/Yocto/poky-hardblare/build/tmp/deploy/sdk]$ sh poky-musl-x86_64-core-image-minimal-cortexa9hf-neon-toolchain-2.3.2.sh
Poky (Yocto Project Reference Distro) SDK installer version 2.3.2
=================================================================
Enter target directory for SDK (default: /opt/poky/2.3.2): 
The directory "/opt/poky/2.3.2" already contains a SDK for this architecture.
If you continue, existing files will be overwritten! Proceed[y/N]? y
[sudo] password for mounir: 
Extracting SDK....................................................................................done
Setting it up...done
SDK has been successfully set up and is ready to be used.
Each time you wish to use the SDK in a new shell session, you need to source the environment setup script e.g.
 $ . /opt/poky/2.3.2/environment-setup-cortexa9hf-neon-poky-linux-musleabi

```


## Using the SDK

The first thing to do for using the SDK toolchains, is to source the environnement setup file with the command bellow :

```
[~/HardBlare]$ source /opt/poky/2.3.2/environment-setup-cortexa9hf-neon-poky-linux-musleabi
```

The environnement will contain the ${CLANGCC} variable for using the clang compiler with right arguments for the target.
Then, we can cross compile a user application with the command line bellow.

```
[~/]$ cd ~/HardBlare/hardblare-use-cases/tests/lsm_hooks/mallocFree
[~/HardBlare/hardblare-use-cases/tests/lsm_hooks/mallocFree] ${CLANGCC} mallocFree.c
```

For performing the static analysis, generating annotations and instrumentation, add the command line option -mllvm -hardblare.

```
[~/HardBlare/hardblare-use-cases/tests/lsm_hooks/mallocFree] ${CLANGCC} -mllvm -hardblare mallocFree.c
```

For statically compiling the same program :

```
[~/HardBlare/hardblare-use-cases/tests/lsm_hooks/mallocFree] ${CLANGCC} -mllvm -hardblare -static-libgcc mallocFree.c
```

## Source code of use cases

For use cases and annotations generator, the SDK should be deployed.
Clone this repository into **~/HardBlare/**.

```shell
[~/]$ cd ~/HardBlare/ 
[~/HardBlare]$ git clone git@gitlab.inria.fr:blare/hardblare/hardblare-use-cases.git
```

Branches :

| Name   | Status                                                       | Description                                   |
| ------ | ------------------------------------------------------------ | --------------------------------------------- |
| master | ![alt text](https://img.shields.io/badge/build-passing-green.svg "Build") ![alt text](https://img.shields.io/badge/working-stable-green.svg "Build") | The only branch containing the stable version | 

## How to use the annotations generator

The annotations generator can be used to manually generate annotations and using them into an executable. 
The annotation generator consists of an HTML page located at **AnnotationsGenerator/index.html**
![](img/annotations_generator.png)
We can the inject the annotations directly into the assembly file of your user app.
For example when compiling a program with the command bellow:

```
[~/HardBlare/hardblare-use-cases/Results-thesis/SYSTEM/mallocFree] ${CLANGCC} -S -mllvm -hardblare mallocFree.c
```

The assembly file *mallocFree.s* will be created and we can directly inject annotations into it.
For example, the first basic block of the function called *useAfterFree* has as a symbol *PTM_useAfterFree_0*.
First, into the Basic Block Table section called *.HB.bbt*, we put the symbol of the basic block.


```assembly

	.section	.HB.bbt,"a",%progbits
	.long	.PTM_useAfterFree_0

```
Then, we are creating a label *(here .Ltmp0)* into the annotations section called *.HB.annot*, in order to assign to this label the absolute address into the .HB.annot section.
We also write the number of annotations contained for the basic block, here 3 annotations are relatives to the basic block *.PTM_useAfterFree_0*.

```assembly
	.section	.HB.annot,"a",%progbits
.Ltmp0:
	.long	3
```

Then we are going back to the Basic Block Table section called *.HB.bbt* in order to write the relative address of the label created *(.Ltmp0)*

```assembly
	.section	.HB.bbt,"a",%progbits
	.long	.Ltmp0(sbrel)
```

And finaly, we are going back to annotations section called *.HB.annot* in order to inject the three annotations (the integer values) relatives to the basic block and generated thanks to the [annotations generator](#annotations-generator).  

```assembly
	.section	.HB.annot,"a",%progbits
	.long	504430588
	.long	504365060
	.long	502136832
```

After injecting annotation into the assembly file,

